'use strict';

/**
 * Populate a Postgresql Table
 */

// modules
const pgp = require('pg-promise')({
	capSQL: true // capitalize all generated SQL
});

// config
const env = require('./env.json');
const TABLE_SIZE = 100;
// CLI args (source / destination)
let DST_TABLE = null;
const SCHEM = [];
if (process.argv.length > 2) {
	DST_TABLE = process.argv[2];
	if (process.argv.length > 3) {

		for (let i=3; i < process.argv.length; i++) {
			SCHEM.push(process.argv[i]);
		}
	}
} else {
	usage();
	process.exit(1);
}

function usage() {
	const programName = process.argv[1].substring(process.argv[1].lastIndexOf('/') + 1);
	console.log('Usage: ' + programName + ' dest_table' + ' param1 param2 ...');
	console.log('\tdest_table:\t\tname of the PostgreSQL table to create and populate');
	console.log('\tparam n:\t\tname of a specific column');
}

// PostgreSQL with pg-promise
const psql = pgp("postgres://" + env.postgres.username + ":" + env.postgres.password + "@" + env.postgres.host + ":" + env.postgres.port + "/" + env.postgres.databaseName);
console.log('connected to PostgreSQL');



// loads data from PostgreSQL by slices of 10
async function createTable(size) {
	let paramstring = "";
	if (SCHEM[0]!=null) { 		
		for (let i=0 ; i < SCHEM.length ; i++) {
			paramstring = paramstring + ',' +  SCHEM[i] + ' varchar(50)'
		}
	}

	try {
		const data = await psql.any('CREATE TABLE ' + DST_TABLE +  ' ( _id serial PRIMARY KEY, prim_val varchar(50)' + paramstring + ')');
		console.log(data);

/*
 TODO POPULATE THE TABLE
*/		

		var randomdata = generateData();
		console.log(randomdata);
		let paramsarray = ['prim_val'];

		if (SCHEM[0]!=null) { 		
			for (let i=0 ; i < SCHEM.length ; i++) {

				paramsarray.push(SCHEM[i]);
			}
		}

		const query = pgp.helpers.insert(randomdata, paramsarray, DST_TABLE);	
		
		await psql.none(query);	
		
	} catch(e) {
			console.log('Error in creating table: ' + e);
	}
console.log('done');





}


function makeString(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}


function generateData() {

	var data_array = [];

	for (let i=0; i<TABLE_SIZE;i++) {
		let newelement = {prim_val : makeString(5)} ;

		if (SCHEM[0]!=null){	
			for (let j=0 ; j < SCHEM.length ; j++) {
				newelement[SCHEM[j]] = makeString(5);

			}
		}
		console.log(newelement);
		data_array.push(newelement);
	
	}

	return data_array;
}


createTable(TABLE_SIZE)
.then(() => {
	console.log('all good :)');
	//process.exit(0);
})
.catch(() => {
	console.log('something went wrong :/');
	//process.exit(1);
});
	
	


