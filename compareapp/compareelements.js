'use strict'

const nano = require('nano');
const _ = require('underscore');
const arangojs = require('arangojs').Database;
const colors = require('colors');
const fs = require('fs');

// ------- ------- ------- ------- ------- ------- -------

const env = require('./env.json');
const arango_db = new arangojs({
	url: `http://${env.arango.username}:${env.arango.password}@${env.arango.host}:${env.arango.port}`,
	databaseName: env.arango.arango_database
});
const OUTPATH = './output';


// check if the two names required are here
let COLL_1 = null;
let COLL_2 = null;

if (process.argv.length == 4) {
	COLL_1 = process.argv[2];
	COLL_2 = process.argv[3];
} else {
	usage();
	process.exit(1);
}

function usage() {
	const programName = process.argv[1].substring(process.argv[1].lastIndexOf('/') + 1);
	console.log('Usage: ' + programName + ' coll1 coll2');
	console.log('\tgoal:\t\tcompare documents in two arango collections');
}

// check if some keys exists in the first table but not in the second table.
async function checkUniqueEntry (coll1, coll2) {
	let cursor;
	let results;
	try {
		cursor = await arango_db.query(`FOR c IN ${coll1}
						LET checkList = (
							FOR d IN ${coll2}
							FILTER c._key == d._key
							RETURN 1
						)
						FILTER LENGTH(checkList) == 0
						RETURN {key : c._key}`);
		results = await cursor.all();
	}
	catch(e) {
		console.log('Error in Arango query: ' + e);
		console.log('Exiting process 1');
		process.exit();
	} 
//	console.log(results);
	return results;
	}


async function compareCommonEntries (coll1, coll2) {
	let cursor;
	let results;
	try {

	 
		cursor = await arango_db.query(`FOR c IN ${coll1}
						FOR d IN ${coll2}
							FILTER c._key==d._key
							LET doc1 = c
							LET doc2 = d
							LET missing = ( FOR key IN ATTRIBUTES(doc1)
								FILTER ! HAS(doc2, key)
								RETURN {[ key ]: doc1[key]}
							)
							LET changed = ( FOR key IN ATTRIBUTES(doc1)
								FILTER HAS(doc2, key) && doc1[key] != doc2[key] && key !="_rev" && key!="_id"
								RETURN {[ key ] : {source: doc1[key],dest: doc2[key]}}
							)
							LET added = (   FOR key IN ATTRIBUTES(doc2)
								FILTER ! HAS(doc1, key)
								RETURN {[ key ]: doc2[key]}
							)
							FILTER LENGTH(missing)!=0 || LENGTH(changed)!=0 || LENGTH(added)!=0
							RETURN { "key" : c._key,
								"missing_attr_in_dest": missing,
								"value_change_for_same_attribute": changed,
								"missing_attr_in_source": added
							}`);
					
		results = await cursor.all();

	}
	catch(e) {
		console.log('Error in Arango query: ' + e);
		console.log('Exiting process 2');
		process.exit();
	} 
//	console.log(JSON.stringify(results));
	return results;
	}
	
	
	
// scanning andchecking two tables to compare entries by primary key
async function scanTables(source, dest) {

	try {
	let extra_in_source = await checkUniqueEntry(source, dest);
	let extra_in_dest = await checkUniqueEntry(dest, source);
	let diff_in_same_key = await compareCommonEntries(source, dest);

	
	
	console.log('Keys of elements existing in source ' + source + ' but not in destination ' + dest + ' :');
	console.log(extra_in_source);
	console.log('Keys of elements existing in destination ' + dest + ' but not in source ' + source + ' :');
	console.log(extra_in_dest);
	console.log('Elements existing in both source and destination, but having different attributes and values :'); 
	console.log(JSON.stringify(diff_in_same_key));
	
// create output on disk
	// create a folder dedicated to the two tables. If folder and files already exists, they will be overwriten.
	fs.mkdir(OUTPATH + "/" + source + "_to_" + dest,{ recursive: true }, function(err) {
		if (err) {
			console.log(err)
		} else {
			console.log("Directory successfully created or was already existing.")
	// create first file with keys missing in dest
			fs.writeFile( OUTPATH + "/" + source + "_to_" + dest + '/missing_in_dest.txt', JSON.stringify(extra_in_source), function (err) {
	  			if (err) throw err;
		 		console.log('Creating file concerning keys missing in destination');
			}); 
	// create second file with keys missing in source
			fs.writeFile(OUTPATH + "/" + source + "_to_" + dest + '/missing_in_source.txt', JSON.stringify(extra_in_dest), function (err) {
  				if (err) throw err;
 				console.log('Creating file concerning keys missing in source');
			}); 
	// create third file with values changes and attributes issues
			fs.writeFile(OUTPATH + "/" + source + "_to_" + dest + '/changes_on_common_keys.txt', JSON.stringify(diff_in_same_key), function (err) {
  				if (err) throw err;
 				console.log('Creating file concerning changes about values and attributes for keys shared by both tables');
			}); 
			
		}
	})
	
	}
	catch(e) {
		console.log('Error in Arango query: ' + e);
		console.log('Exiting process 3');
		process.exit();
	} 
	
}


//checkUniqueEntry(COLL_1, COLL_2);
//compareCommonEntries (COLL_1, COLL_2);

	
scanTables(COLL_1, COLL_2).then(() => {
	console.log('process finished');
	//process.exit(0);
})
.catch(() => {
	console.log('something went wrong :/');
	//process.exit(1);
});






















