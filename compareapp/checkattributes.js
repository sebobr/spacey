'use strict'

const nano = require('nano');
const _ = require('underscore');
const arangojs = require('arangojs').Database;
const colors = require('colors');

// ------- ------- ------- ------- ------- ------- -------

const env = require('./env.json');
const arango_db = new arangojs({
	url: `http://${env.arango.username}:${env.arango.password}@${env.arango.host}:${env.arango.port}`,
	databaseName: env.arango.arango_database
});

// check if the two names required are here
let COLL_1 = null;
let COLL_2 = null;

if (process.argv.length == 4) {
	COLL_1 = process.argv[2];
	COLL_2 = process.argv[3];
} else {
	usage();
	process.exit(1);
}

function usage() {
	const programName = process.argv[1].substring(process.argv[1].lastIndexOf('/') + 1);
	console.log('Usage: ' + programName + ' coll1 coll2');
	console.log('\tgoal:\t\tcompare attributes of rows in two arango collections');
}


async function checkAttributes (coll) {
	let cursor;
	cursor = await arango_db.query(`LET attributesPerDocument = (
						FOR doc IN ${coll} RETURN ATTRIBUTES(doc, true)
					)
					RETURN MERGE(
					FOR attributeArray IN attributesPerDocument
						FOR attribute IN attributeArray
							COLLECT attr = attribute WITH COUNT INTO count
							SORT count DESC
							RETURN {[attr] : count} )`);
	const results = await cursor.all();
	return results[0];
	}


async function compareAttributes (coll1, coll2) {
	let attr_table1 = await checkAttributes(coll1);
	console.log(attr_table1);
	let attr_table2 = await checkAttributes(coll2);
	console.log(attr_table2);
	var diverge1 = [];
	var diverge2 = [];
	var converge = [];
	
	for (let val in attr_table1) {
    		if (attr_table2.hasOwnProperty(val)) {converge.push(val)}
    		else {diverge1.push(val)}
	}
	for (let val in attr_table2) {
    		if (attr_table1.hasOwnProperty(val) != true) {diverge2.push(val)}
	}

	console.log('Tables have columns in common : ');
	console.log(converge);
	console.log(coll1 + ' have unique parameters : ');
	console.log(diverge1);	
	console.log(coll2 + ' have unique parameters : ');
	console.log(diverge2); 
	return 'OK';

}

compareAttributes (COLL_1, COLL_2).then(() => {
	console.log('process finished well');
	//process.exit(0);
})
.catch(() => {
	console.log('something went wrong :/');
	//process.exit(1);
});





