# Table Similarity
Let's consider a company called SpaceY. SpaceY is a marketplace selling spaceships.

SpaceY is a mature data company and relies on data to make business decisions.
You are part of the engineering team and you're asked to work on designing and implementing a new feature. 

SpaceY has a micro service architecture and has many databases.
The analytic team is using BigQuery as the main data warehouse, all the insights are built on top of it. 
Your fellow colleagues have already built data pipelines that sync the different databases into the data warehouse.

Your job is to design a new service that will compare two tables (one in source the other in destination) and outputs the differences between them if any. 
## Challenge
- You need to define what aspects are relevant to compare tables accurately (schema, row count, etc...).
- Your solution must support comparing two datasets that can be (very) large. 
- The comparison process must run within minutes and not hours. 

## Requirement
- For the sake of simplicity you can consider the source database and the destination to be the same (you wont need to use two drivers). You are free to go with the database of you choice.  
- Use the back-end technology/language of your choice to implement your solution.
- Package your implementation as a docker.
- Share your solution as a merge request to this repository.
